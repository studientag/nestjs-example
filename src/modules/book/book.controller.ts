import {
  Body,
  Controller,
  Get,
  Param,
  ParseIntPipe,
  Post,
} from '@nestjs/common';
import { TBook } from '../../types/TBook';
import BookCreateDto from './dtos/book.create-dto';
import BookService from './book.service';

@Controller('book')
export class BookController {
  private bookService: BookService;

  constructor(bookService: BookService) {
    this.bookService = bookService;
  }

  @Get('/all')
  getAll(): TBook[] {
    return this.bookService.getAll();
  }

  @Get('/:bookId')
  getSingle(@Param('bookId', new ParseIntPipe()) bookId: number): TBook {
    return this.bookService.getSingle(bookId);
  }

  @Post()
  create(@Body() book: BookCreateDto): TBook {
    return this.bookService.create(book);
  }
}
