import {
  ArrayMaxSize,
  ArrayMinSize,
  IsArray,
  IsDateString,
  IsNumber,
  IsOptional,
  IsString,
  MaxLength,
  ValidateNested,
} from 'class-validator';
import { Type } from 'class-transformer';

export default class BookCreateDto {
  @IsString()
  @MaxLength(1024)
  title: string;

  @IsDateString()
  @IsOptional()
  releaseDate: string;

  @IsArray()
  @ArrayMaxSize(500)
  @ArrayMinSize(1)
  @ValidateNested({ each: true })
  @Type(() => CreateBookPage)
  pages: CreateBookPage[];
}

class CreateBookPage {
  @IsString()
  @MaxLength(1024)
  chapter: string;

  @IsNumber()
  pageNr: number;

  @IsString()
  @MaxLength(1024)
  content: string;
}
