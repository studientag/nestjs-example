import { Injectable, NotFoundException } from '@nestjs/common';
import { TBook } from '../../types/TBook';

@Injectable()
export default class BookService {
  private readonly books: TBook[] = [];

  getAll(): TBook[] {
    return this.books;
  }

  getSingle(id: number): TBook {
    const book = this.books.find((book) => book.id === id);
    if (!book) throw new NotFoundException();

    return book;
  }

  create(book: TBook): TBook {
    this.books.push(book);
    return book;
  }
}
