export type TBook = {
  id?: number;
  title: string;
  releaseDate?: string;
  pages: TBookPage[];
};

export type TBookPage = {
  chapter: string;
  pageNr: number;
  content: string;
};
